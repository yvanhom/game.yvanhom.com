### Information

**Home Page**: [itch.io](https://unwontedstudios.itch.io/written-in-the-sky), [steam](http://store.steampowered.com/app/416130/)

**Author**: [Unwonted Studios](https://unwontedstudios.itch.io/)

**Type**: Visual Novel

**Average duration**: About an hour

**Descriptions**:

As the daughter of a detective, Azure has always been a curious child.

When she finds something, or someone, out of the ordinary, she has a compulsion to watch on and make sense of what she's seen.

But on this particular night, Azure's curiosity proves to be her undoing.

After stalking a suspicious figure through the night, Azure stumbles across an odd, black box, bearing alien writing. And within that box she finds a small ring, just her size, with writing matching that of the box. Without thinking, Azure picks up the ring, and the moment she does, footsteps appear behind her.

Panicked, Azure flees the scene. She runs home to safety, examines the ring, and wonders why she risked her safety for such a small trinket. But the moment she puts the ring on, Azure realizes she's gotten more than she bargained for.

The footsteps belonged to a small alien girl, Sienna, who explains that the ring cannot be removed. It is an alien mating ring stolen by the FBI during an intergalactic peace conference. The ring is the key to bringing peace to the planets, and as the daughter of the emperor of Mars, Sienna came to earth to retrieve it.

But the ring has already chosen its host, Azure, and is now bound to her forever. And with it, Sienna too has been bound to the human girl, fated to be her lover.

### My translations

**LICENSE**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git repo**: [bitbucket](https://bitbucket.org/yvanhom/writteninthesky-cn)

**Download**: [bitbucket downloads](https://bitbucket.org/yvanhom/writteninthesky-cn/downloads)

**Instructions**:

1. Download the game from [itch.io](https://unwontedstudios.itch.io/written-in-the-sky), or install from [steam](http://store.steampowered.com/app/416130/), and extract the tarball.
2. Download the translation patch (named WrittenInTheSky-cnpatch-VERSION.zip) from [bitbucket downloads](https://bitbucket.org/yvanhom/writteninthesky-cn/downloads), extract the tarball.
3. Merge the game directory and the path directory, by keeping the "game" directory in the same level. However, no files overwriting.



