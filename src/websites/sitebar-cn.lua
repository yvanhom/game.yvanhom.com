return {
    {
        index = "已完成：",
        {
            key = "Her Tears Were My Lights",
            value = "her-tears-were-my-light-cn.html",
        },
        {
            key = "The Sad Story of Emmeline Burns",
            value = "the-sad-story-of-emmeline-burns-cn.html"
        },
        {
            key = "Romance Detective",
            value = "romance-detective-cn.html"
        },
        {
            key = "Princesses's Maid",
            value = "princess-maid-cn.html",
        },
        {
            key = "Written in the Sky",
            value = "written-in-the-sky-cn.html",
        },
    },
    {
        index = "进展中：",
        {
            key = "A Little Lily Princess",
            value = "#",
        },
    },
    {
        index = "网站语言：",
        {
            key = "英语",
            value = "index.html",
        },
        {
            key = "中文",
            value = "index-cn.html",
        },
    }
}
