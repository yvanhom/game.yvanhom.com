### 信息

**游戏主页**: [itch.io](https://nomnomnami.itch.io/romance-detective)

**游戏作者**: [Nami](https://nomnomnami.itch.io/)

**游戏类型**: 视觉小说

**平均时长**: 大约一半个小时

**游戏描述**:

浪漫侦探是视觉小说游戏，最初是制作参加 NaNoRenO 2014，讲述解决激情犯罪的侦探跟她的搭档浪漫警官的关于爱的故事。

### 翻译

**中文名**: 浪漫侦探

**共享协议**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git库**: [bitbucket](https://bitbucket.org/yvanhom/romancedetective-cn/)

**下载**: [bitbucket downloads](https://bitbucket.org/yvanhom/romancedetective-cn/downloads)

**操作指南**:

1. 从 [itch.io](https://nomnomnami.itch.io/romance-detective) 下载游戏，并解压压缩包。
2. 从 [bitbucket downloads](https://bitbucket.org/yvanhom/romancedetective-cn/downloads) 下载中文补丁文件，文件名为 HTWML-cnpatch-VERSION.zip，解压补丁压缩包。
3. 将补丁文件夹跟游戏文件夹合并，注意保持两个的 game 目录合并，但没有文件的覆盖。
