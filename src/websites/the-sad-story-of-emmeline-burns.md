### Information

**Home Page**: [steam](http://store.steampowered.com/app/429940/), [itch.io](https://ebihime.itch.io/emmeline)

**Author**: [Ebi-Hime](https://ebihime.itch.io/)

**Type**: Visual Novel

**Average duration**: About one and a half hour

**Descriptions**:

Toma Andrews, aged 14, has always felt like an outsider. She enjoys spending time in graveyards, and feels more comfortable among the dead than the living.

That is, until Toma meets a strange girl in her local graveyard who might be a real ghost.

The girl asks Toma if she will stay for a while and listen to a story. A story about a girl called Emmeline Burns, who died a tragic death back in 1851.

Toma is compelled to listen, but at the same time, she feels uneasy. After all, what can she do? How could she ever hope to help somebody who has already died?

### My translations

**LICENSE**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git repo**: [bitbucket](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn)

**Download**: [bitbucket downloads](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn/downloads)

**Instructions**:

1. Download the game from [itch.io](https://ebihime.itch.io/emmeline), and extract the tarball. Or install from [steam](http://store.steampowered.com/app/429940/). The latter is recommended.
2. Download the translation patch (named TSSoEB-cnpatch-VERSION.zip) from [bitbucket downloads](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn/downloads), extract the tarball.
3. Merge the game directory and the path directory, by keeping the "game" directory in the same level, overwriting some files.
