### 信息

**游戏主页**: [steam](http://store.steampowered.com/app/429940/), [itch.io](https://ebihime.itch.io/emmeline)

**游戏作者**: [Ebi-Hime](https://ebihime.itch.io/)

**游戏类型**: 视觉小说

**平均时长**: 大约一半个小时

**游戏描述**:

托玛·安德鲁斯，14岁，一直像个外来者，喜欢花时间呆在墓地里，感觉呆在死者中间比呆在活人中间更舒适。

直到有一天，托玛在当地的墓地里，邂逅了一个古怪的女孩，她可能是真正的幽灵。

那女孩邀请托玛逗留片刻，听她讲个故事，关于一个叫艾米琳·伯恩斯的女孩的故事，她在1851年的时候悲惨地死去。

托玛不由自主地沉迷，但同时她感觉到不安。毕竟，她能做什么呢？她怎样能够帮助到一个已经死去的人？

### 翻译

**中文名**: 幽灵悲情 / 艾米琳·伯恩斯的悲惨往事

**共享协议**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git库**: [bitbucket](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn)

**下载**: [bitbucket downloads](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn/downloads)

**操作指南**:

1. 从 [itch.io](https://ebihime.itch.io/emmeline) 下载游戏，并解压压缩包。或者直接从 [steam](http://store.steampowered.com/app/429940/) 安装游戏。更建议后者。
2. 从 [bitbucket downloads](https://bitbucket.org/yvanhom/thesadstoryofemmelineburns-cn/downloads) 下载中文补丁文件，文件名为 TSSoEB-cnpatch-VERSION.zip，解压补丁压缩包。
3. 将补丁文件夹跟游戏文件夹合并，注意保持两个的 game 目录合并，有一些文件需要覆盖。
