### 信息

**游戏主页**: [itch.io](https://toki-production.itch.io/princessess-maid)

**游戏作者**: [Toki Production](https://toki-production.itch.io/)

**游戏类型**: 视觉小说

**平均时长**: 大约半个小时

**游戏描述**:

The twin princesses are going to get married to people they don't even know in 13 days for the sake of their kingdom. What can their childhood friend, a mere powerless maid do? 

孪生公主姐妹们即将在13天后为了她们的王国嫁给她们完全不认识的人。她们才小到大的朋友，区区一个无能为力的女仆，能做什么呢？

### 翻译

**中文名**: 公主女仆

**共享协议**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git库**: [bitbucket](https://bitbucket.org/yvanhom/princessmaid-cn/)

**下载**: [bitbucket downloads](https://bitbucket.org/yvanhom/princessmaid-cn/downloads)

**操作指南**:

1. 从 [itch.io](https://toki-production.itch.io/princessess-maid) 下载游戏，并解压压缩包。
2. 从 [bitbucket downloads](https://bitbucket.org/yvanhom/princessmaid-cn/downloads) 下载中文补丁文件，文件名为 PrincessMaid-cnpatch-VERSION.zip，解压补丁压缩包。
3. 将补丁文件夹跟游戏文件夹合并，注意保持两个的 game 目录合并，但没有文件的覆盖。
