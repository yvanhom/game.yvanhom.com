### 信息

**游戏主页**: [itch.io](https://unwontedstudios.itch.io/written-in-the-sky), [steam](http://store.steampowered.com/app/416130/)

**游戏作者**: [Unwonted Studios](https://unwontedstudios.itch.io/)

**游戏类型**: 视觉小说

**平均时长**: 大约一个小时

**游戏描述**:

作为侦探的女儿，艾瑞莉一直是个好奇心旺盛的女孩。

当她见到不同寻常的事物，她就有股冲动，想要密切关注，弄清她所见到的究竟是什么。

但在这个特殊的夜晚，艾瑞莉的好奇心却为她埋下了祸根。

她跟踪着一可疑的身影，偶然见到一刻着陌生文字的奇异的黑盒子，盒子里，她找到一枚小戒指，上面同样刻着陌生的文字。艾瑞莉不加思索就把戒指戴上，但这时，她身后传来脚步声。

受到惊吓的艾瑞莉夺步而逃。她跑回家中，检查着戒指，困惑她为何要为这件小饰物而担上危险。那是FBI从星际和平会议偷来的外星婚戒，它是给那些星球带来和平的关键。作为火星帝王的女儿，西恩娜来到地球进行寻找。

但戒指已经选好了她的主人，艾瑞莉，并跟她永远地绑定在一起。因为这，西恩娜也跟这命中注定是她的爱人的人类少女产生了羁绊。


### 翻译

**中文名**: 书之天宇

**共享协议**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git库**: [bitbucket](https://bitbucket.org/yvanhom/writteninthesky-cn)

**下载**: [bitbucket downloads](https://bitbucket.org/yvanhom/writteninthesky-cn/downloads)

**操作指南**:

1. 从 [itch.io](https://unwontedstudios.itch.io/written-in-the-sky) 下载游戏，并解压压缩包，或是从 [steam](http://store.steampowered.com/app/416130/) 直接安装。
2. 从 [bitbucket downloads](https://bitbucket.org/yvanhom/writteninthesky-cn/downloads) 下载中文补丁文件，文件名为 WrittenInTheSky-cnpatch-VERSION.zip，解压补丁压缩包。
3. 将补丁文件夹跟游戏文件夹合并，注意保持两个的 game 目录合并，但没有文件的覆盖。
