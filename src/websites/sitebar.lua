return {
    {
        index = "Accomplished",
        {
            key = "Her Tears Were My Lights",
            value = "her-tears-were-my-light.html",
        },
        {
            key = "The Sad Story of Emmeline Burns",
            value = "the-sad-story-of-emmeline-burns.html"
        },
        {
            key = "Romance Detective",
            value = "romance-detective.html"
        },
        {
            key = "Princesses's Maid",
            value = "princess-maid.html",
        },
        {
            key = "Written in the Sky",
            value = "written-in-the-sky.html",
        },
    },
    {
        index = "In progress",
        {
            key = "A Little Lily Princess",
            value = "#",
        },
    },
    {
        index = "Website languages",
        {
            key = "English",
            value = "index.html",
        },
        {
            key = "Chinese",
            value = "index-cn.html",
        },
    }
}
