return {
    english = {
        siteowner = "Yvan Hom",
        ownermail = "yvanhom@gmail.com",
        sitetitle = "Yuri Games Yvan Hom Translates",
        siteyear = "2016",
        sitebarname = "sitebar",
    },
    chinese = {
        siteowner = "Yvan Hom",
        ownermail = "yvanhom@hotmail.com",
        sitetitle = "Yvan Hom所翻译的百合游戏",
        siteyear = "2016",
        sitebarname = "sitebar-cn",
    }
}
