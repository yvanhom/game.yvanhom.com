### Information

**Home Page**: [itch.io](https://nomnomnami.itch.io/romance-detective)

**Author**: [Nami](https://nomnomnami.itch.io/)

**Type**: Visual Novel

**Average duration**: About one and a half hour

**Descriptions**:

ROMANCE DETECTIVE is a visual novel originally made for NaNoRenO 2014. it's a love story about a detective who solves crimes of passion alongside her partner, romance cop.

### My translations

**LICENSE**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git repo**: [bitbucket](https://bitbucket.org/yvanhom/romancedetective-cn)

**Download**: [bitbucket downloads](https://bitbucket.org/yvanhom/romancedetective-cn/downloads)

**Instructions**:

1. Download the game from [itch.io](https://nomnomnami.itch.io/romance-detective), and extract the tarball.
2. Download the translation patch (named RD-cnpatch-VERSION.zip) from [bitbucket downloads](https://bitbucket.org/yvanhom/romancedetective-cn/downloads), extract the tarball.
3. Merge the game directory and the path directory, by keeping the "game" directory in the same level. However, no files overwriting.
