return function(area)
    local text = {
        "\n",
    	[[<nav id="nav">]],
        "    <ul>"
    }
    
    if area.index then
        table.insert(text,  string.format([[        <li class="current">%s</li>]], area.index))
    end
    
    for i, v in ipairs(area) do
        table.insert(text, string.format([[        <li><a href="%s">%s</a></li>]], v.value or "#", v.key))
    end
    
    table.insert(text, "    </ul>")
    table.insert(text, "</nav>\n")
    return table.concat(text, "\n")
end
