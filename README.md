All files related to my site [http://yvanhom.bitbucket.org].

### Build ###

```bash
git clone git@bitbucket.org:yvanhom/yvanhom.bitbucket.org.git
git submodule init
git submodule update
cd src
lua yvgensite.lua websites template ..
lua yvgensm.lua websites http://yvanhom.bitbucket.org ../
```
